
/**
 * @file signal.h
 * @brief Header file for the mads_signal_t structure and its associated functions.
 *
 * This file provides the definition of the mads_signal_t data structure, which
 * is used to create and manage signals in the MADS framework. Signals are a 
 * mechanism for facilitating communication between different components of a program,
 * enabling event-driven programming and decoupled architecture.
 *
 * This file offers the following key features:
 * - Definition of the mads_signal_t data structure for managing signal connections 
 *   and ensuring thread safety via a mutex.
 * - Functions to create, clear, and free signals.
 * - Thread-safe operations for managing signal connections.
 *
 * @note This header file is designed to be compatible with both C and C++.
 */

#ifndef MADS_SIGNALS_SIGNAL_H
#define MADS_SIGNALS_SIGNAL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <threads.h>
#include <mads_export.h>
#include <mads/data_structures/array.h>

/**
 * @brief Data structure that represents a mads signal.
 */
typedef struct
{
    mtx_t mutex; ///< @brief mutex for thread safety
    mads_array_t *connections; ///< @brief mads array of connections to the signal
} mads_signal_t;


/**
 * @brief Function to create a new signal.
 * @return Pointer to a created signal
 */
MADS_EXPORT mads_signal_t *mads_signal_create();

/**
 * @brief Function to clear (remove) all connections to the signal.
 * @param[in,out] signal Signal to be cleared of connections
 */
MADS_EXPORT void mads_signal_clear(const mads_signal_t *signal);

/**
 * @brief Function to free a signal and all allocated memory.
 * @param[in,out] signal The signal to free
 */
MADS_EXPORT void mads_signal_free(mads_signal_t **signal);

#ifdef __cplusplus
}
#endif


#endif //MADS_SIGNALS_SIGNAL_H
