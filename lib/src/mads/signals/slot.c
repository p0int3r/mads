// ReSharper disable CppParameterMayBeConstPtrOrRef
// ReSharper disable CppRedundantCastExpression



#include <assert.h>
#include <stdlib.h>
#include <mads/signals/slot.h>


// The function for creating a new slot data structure. It takes a pointer
// to a context object and a function pointer to a slot function and allocates
// memory for the data structure and all of its components.
mads_slot_t *mads_slot_create(void *context, const mads_slot_fn function)
{
    // Check context and the function are provided. If not, throw an assertion error.
    assert(context != NULL && function != NULL);

    // Allocate memory for the slot. If memory allocation fails, an assertion error is thrown.
    mads_slot_t *slot = (mads_slot_t *)malloc(sizeof(mads_slot_t));
    assert(slot != NULL);

    // Set the slot to not block.
    slot->blocked = 0;

    // Set the context object associated with the slot.
    slot->context = context;

    // Set the function pointer associated with the slot.
    slot->function = function;

    // Return the newly created slot.
    return slot;
}

// The function that frees the mads slot data structure.
void mads_slot_free(mads_slot_t **slot)
{
    // Check if the given slot is NULL. If it is, throw an assertion errror.
    assert(*slot != NULL);

    // Set context and function to NULL.
    (*slot)->context = NULL;
    (*slot)->function = NULL;

    // Free the slot and set it to NULL.
    free(*slot);
    *slot = NULL;
}
